package com.ardab.subsapp.mobilepackage.rest;

import com.ardab.subsapp.mobilepackage.model.MobilePackage;
import javassist.NotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

public interface MobilePackageRestService {

    public ResponseEntity<MobilePackage> createNewPackage(MobilePackage mobilePackage, HttpServletRequest request);
    public ResponseEntity<MobilePackage> updatePackage(int packageId, MobilePackage mobilePackage) throws NotFoundException;
    public ResponseEntity<MobilePackage> deletePackage(int packageId) throws NotFoundException;
    public ResponseEntity<List<MobilePackage>> getAllPackages();
    public ResponseEntity<MobilePackage> getPackageById(int id) throws NotFoundException;

}
