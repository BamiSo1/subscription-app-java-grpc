package com.ardab.subsapp.mobilepackage.rest;

import com.ardab.subsapp.mobilepackage.model.MobilePackage;
import com.ardab.subsapp.mobilepackage.repository.MobilePackageRepository;
import javassist.NotFoundException;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Service
public class MobilePackageRestServiceImpl implements MobilePackageRestService {

    @Autowired
    MobilePackageRepository mobilePackageRepository;

    @Override
    public ResponseEntity<MobilePackage> createNewPackage(MobilePackage mobilePackage, HttpServletRequest request) {
        MobilePackage newMobilePackage = mobilePackageRepository.saveAndFlush(mobilePackage);
        HttpHeaders responseHeaders = new HttpHeaders();
        responseHeaders.set("Location", mobilePackageUrlHelper(newMobilePackage, request));
        return new ResponseEntity<MobilePackage>(newMobilePackage,responseHeaders, HttpStatus.CREATED);
    }

    @Override
    public ResponseEntity<MobilePackage> updatePackage(int packageId, MobilePackage mobilePackage) throws NotFoundException {
        if(isPackageExist(packageId)){
            MobilePackage existingMobilePackage = mobilePackageRepository.getById(packageId);
            BeanUtils.copyProperties(mobilePackage,existingMobilePackage);
            existingMobilePackage.setId(packageId);
            MobilePackage updatedMobilePackage = mobilePackageRepository.saveAndFlush(existingMobilePackage);
            return new ResponseEntity<MobilePackage>(updatedMobilePackage, HttpStatus.OK);
        }
        else {
            throw new NotFoundException(((Integer) packageId).toString());
        }
    }

    @Override
    public ResponseEntity<MobilePackage> deletePackage(int packageId) throws NotFoundException {
        if (isPackageExist(packageId)){
            mobilePackageRepository.deleteById(packageId);
            return new ResponseEntity<MobilePackage>(HttpStatus.NO_CONTENT);
        }else {
            throw new NotFoundException(((Integer) packageId).toString());
        }
    }

    @Override
    public ResponseEntity<List<MobilePackage>> getAllPackages() {
        List<MobilePackage> mobilePackageList = mobilePackageRepository.findAll();
        return new ResponseEntity<List<MobilePackage>>(mobilePackageList,HttpStatus.OK);
    }

    @Override
    public ResponseEntity<MobilePackage> getPackageById(int id) throws NotFoundException {
        if(isPackageExist(id)){
            MobilePackage mobilePackage = mobilePackageRepository.getById(id);
            return new ResponseEntity<MobilePackage>(mobilePackage,HttpStatus.OK);
        }
        else {
            throw new NotFoundException(((Integer) id).toString());
        }
    }

    private String mobilePackageUrlHelper(MobilePackage mobilePackage, HttpServletRequest request){
        StringBuilder resourcePath = new StringBuilder();
        resourcePath.append(request.getRequestURL());
        resourcePath.append("/");
        resourcePath.append(mobilePackage.getId());
        return resourcePath.toString();
    }

    public boolean isPackageExist(int id){
        if(mobilePackageRepository.findById(id).isPresent()){
            return true;
        }
        else {
            return false;
        }
    }
}
