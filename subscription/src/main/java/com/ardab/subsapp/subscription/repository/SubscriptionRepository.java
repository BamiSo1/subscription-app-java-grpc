package com.ardab.subsapp.subscription.repository;

import com.ardab.subsapp.subscription.model.Subscription;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SubscriptionRepository extends JpaRepository<Subscription, Long> {
}
