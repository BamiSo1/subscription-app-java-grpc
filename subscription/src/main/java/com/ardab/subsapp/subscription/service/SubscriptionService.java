package com.ardab.subsapp.subscription.service;

import com.ardab.gprc.mobilepackage.*;
import com.ardab.gprc.subscription.*;
import com.ardab.subsapp.subscription.exception.SubscriptionException;
import com.ardab.subsapp.subscription.model.Subscription;
import com.ardab.subsapp.subscription.repository.SubscriptionRepository;
import com.google.protobuf.Any;
import com.google.protobuf.InvalidProtocolBufferException;
import com.google.rpc.Status;
import io.grpc.StatusRuntimeException;
import io.grpc.protobuf.StatusProto;
import io.grpc.stub.StreamObserver;
import net.devh.boot.grpc.server.service.GrpcService;
import org.springframework.beans.factory.annotation.Autowired;

@GrpcService
public class SubscriptionService extends SubscriptionServiceGrpc.SubscriptionServiceImplBase{

    @Autowired
    SubscriptionRepository subscriptionRepository;

    @Autowired
    PackageCheckService packageCheckService;


    @Override
    public void createNewSubscription(SubscriptionRequest subscriptionRequest, StreamObserver<SubscriptionResponse> responseStreamObserver) {
        PackageResponse packageResponse = packageCheckService.packageDetails(subscriptionRequest.getSubscriptionPackage());
        if(packageResponse.getPackagePrice() <= subscriptionRequest.getSubscriberBalance()){
            Subscription subscription = new Subscription(subscriptionRequest.getSubscriptionPackage(),subscriptionRequest.getSubscriberMsisdn(),0);
            subscriptionRepository.saveAndFlush(subscription);
            SubscriptionResponse subscriptionResponse = SubscriptionResponse.newBuilder()
                    .setSubscriptionStatus(0)
                    .setCollectedPrice(packageResponse.getPackagePrice())
                    .build();
            responseStreamObserver.onNext(subscriptionResponse);
            responseStreamObserver.onCompleted();
        }
        else {
            throw new SubscriptionException(SubscriptionErrorCode.INSUFFICIENT_SUBSCRIBER_BALANCE);
        }

    }
}
